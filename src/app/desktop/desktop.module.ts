import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesktopRoutingModule } from './desktop-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';

import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { DashboardService } from '../services/dashboard.service';

import { DashboardComponent } from './views/dashboard/dashboard.component';
import { BodyComponent } from './views/body/body.component';
import { HeaderComponent } from './views/header/header.component';
import { FooterComponent } from './views/footer/footer.component';
import { FilterService } from '../services/filter.service';
import { ModalDashboardComponent } from './views/dashboard/modal-dashboard/modal-dashboard.component';
import { QuestionService } from '../services/question.service';
import { IndicatorService } from '../services/indicator.service';
import { FavoriteService } from '../services/favorite.service';

@NgModule({
  declarations: [
    DashboardComponent,
    BodyComponent,
    HeaderComponent,
    FooterComponent,
    ModalDashboardComponent
  ],
  imports: [
    CommonModule,
    DesktopRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot()
  ],
  providers: [DashboardService, FilterService, QuestionService, IndicatorService, FavoriteService],
  entryComponents: [ModalDashboardComponent],
})
export class DesktopModule { }
