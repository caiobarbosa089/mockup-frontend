import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { IndicatorService } from 'src/app/services/indicator.service';
import { QuestionService } from 'src/app/services/question.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-modal-dashboard',
  templateUrl: './modal-dashboard.component.html',
  styleUrls: ['./modal-dashboard.component.scss']
})
export class ModalDashboardComponent implements OnInit {
  id: number;
  indicators: Array<any> = [];
  questions: Array<string> = [];

  constructor(
    public bsModalRef: BsModalRef,
    private spinner: NgxSpinnerService,
    private indicatorService: IndicatorService,
    private questionService: QuestionService
    ) {}
 
  ngOnInit() {
    this.spinner.show();
    this.questionService.getQuestion(this.id).subscribe(val => {
      this.spinner.hide();
      if( val && val.data ) this.questions = val.data.questions;
    });
    this.indicatorService.getIndicator(this.id).subscribe(val => {
      if( val && val.data ) this.indicators = val.data.indicators
    });
  }

  teste(e) {
    e.preventDefault();
  }

}
