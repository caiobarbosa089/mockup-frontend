import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import { FilterService } from 'src/app/services/filter.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalDashboardComponent } from './modal-dashboard/modal-dashboard.component';
import { FavoriteService } from 'src/app/services/favorite.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  form: FormGroup = new FormGroup({
    gerencia:       new FormControl('todos'),
    diretoria:      new FormControl('todos'),
    nivelDecisorio: new FormControl('todos'),
    selos: new FormGroup({
      ouro:   new FormControl(false),
      prata:  new FormControl(false),
      bronze: new FormControl(false),
    }),
  });

  bsModalRef: BsModalRef;
  unchangingCatalogs: Array<any> = [];
  unchangingFavorites:Array<any> = [];
  catalogs:           Array<any> = [];
  favorites:          Array<any> = [];
  filters:            Array<any> = []
  allFilters:         Array<any> = [];
  boardFilters:       Array<any> = [];
  managementFilters:  Array<any> = [];

  constructor(
    private dashboardService: DashboardService,
    private favoriteService:  FavoriteService,
    private filterService:    FilterService,
    private spinner: NgxSpinnerService,
    private modalService:     BsModalService
  ) { }
 
  openModalWithComponent(id) {
    const initialState = { id: id };
    this.bsModalRef = this.modalService.show(ModalDashboardComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  ngOnInit() {
    this.spinner.show();
    this.getFilter();
    this.onchanges();

    Promise.all([this.getAllCatalogs(), this.getAllFavorites()])
      .then(values => {
        this.spinner.hide();
        this.unchangingCatalogs = values[0];
        this.catalogs           = values[0];
        this.unchangingFavorites= values[1];
        this.favorites          = values[1];
        this.adaptFavorites(values)
      })
  }

  onchanges(): void {
    this.form.get('diretoria').valueChanges.subscribe(val => {
      this.form.get('gerencia').setValue('todos');
      this.adaptManagementFilters(val, this.allFilters);
    })
    this.form.valueChanges.subscribe(val => {
      this.catalogs = this.filterService.filterData(this.unchangingCatalogs, val)
      this.adaptFavorites([this.catalogs, this.unchangingFavorites]);
    })
  }

  getAllCatalogs(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.dashboardService.getCatalogs().subscribe(val => {
        if(val && val.data) resolve(val.data);
      })
    })
  }

  getAllFavorites(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.favoriteService.getAllFavorites().subscribe(val => {
        if(val && val.data) resolve(val.data);
      });
    });
  }

  getFilter(): void {
    this.filterService.getFilters().subscribe(val => {
      this.allFilters = val.data;
      this.adaptBoardFilters(val.data);
    });
  }

  adaptBoardFilters(filters): void {
    let { boards } = filters;
    for (let item in boards) this.boardFilters.push(item);
  }

  adaptManagementFilters(key, filters): void {
    let { boards } = filters;
    this.managementFilters = [];
    if (key != 'todos') boards[key].forEach(elem => this.managementFilters.push(elem));
  }

  adaptFavorites(values: any) {
    const catalogs: Array<any> = values[0] as Array<any>;
    const favorite:  Array<any> = values[1] as Array<any>;
    this.favorites = catalogs.filter(elemDashboard => favorite.find(elemFavorite => elemFavorite.id == elemDashboard.id))
  }

  accessPanel(item) {
    window.open(item.link, '_blank');
  }
}
