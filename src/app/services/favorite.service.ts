import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {
  private api: string = 'http://localhost:2000';

  constructor(private http: HttpClient) { }

  getAllFavorites(): Observable<any> {
    return this.http
      .get(`${this.api}/favorites`)
      .pipe(
        catchError(
          this.errorHandler('Falha na requisição de catálogos', [])
        )
      );
  }

  private errorHandler<T>(message: string, result: T) {
    return (error: any): Observable<any> => {
      console.error(`${message}: ${error.message}`);
      return of(result as T);
    };
  }
}
