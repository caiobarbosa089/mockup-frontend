import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class IndicatorService {
  private api: string = 'http://localhost:2000';

  constructor(private http: HttpClient) { }

  getAllIndicators(): Observable<any> {
    return this.http
      .get(`${this.api}/indicators`)
      .pipe(
        catchError(
          this.errorHandler('Falha na requisição de indicadores', [])
        )
      );
  }

  getIndicator(id: number): Observable<any> {
    return this.http
      .get(`${this.api}/indicators/${id}`)
      .pipe(
        catchError(
          this.errorHandler('Falha na requisição de indicadores', [])
        )
      );
  }

  private errorHandler<T>(message: string, result: T) {
    return (error: any): Observable<any> => {
      console.error(`${message}: ${error.message}`);
      return of(result as T);
    };
  }
}
