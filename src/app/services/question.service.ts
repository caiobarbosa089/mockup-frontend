import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  private api: string = 'http://localhost:2000';

  constructor(private http: HttpClient) { }

  getAllQuestions(): Observable<any> {
    return this.http
      .get(`${this.api}/questions`)
      .pipe(
        catchError(
          this.errorHandler('Falha na requisição de perguntas', [])
        )
      );
  }

  getQuestion(id: number): Observable<any> {
    return this.http
      .get(`${this.api}/questions/${id}`)
      .pipe(
        catchError(
          this.errorHandler('Falha na requisição de perguntas', [])
        )
      );
  }

  private errorHandler<T>(message: string, result: T) {
    return (error: any): Observable<any> => {
      console.error(`${message}: ${error.message}`);
      return of(result as T);
    };
  }
}
