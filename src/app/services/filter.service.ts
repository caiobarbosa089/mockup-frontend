import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  private api: string = 'http://localhost:2000';

  constructor(private http: HttpClient) { }

  getFilters(): Observable<any> {
    return this.http
      .get(`${this.api}/filters`)
      .pipe(
        catchError(
          this.errorHandler('Falha na requisição de filtros', [])
        )
      );
  }

  filterData(data: any, form: any) {
    let filterData = data;
    filterData = this.boardFilter(filterData, form['diretoria']);
    filterData = this.managementFilter(filterData, form['gerencia']);
    filterData = this.decisionLevelFilter(filterData, form['nivelDecisorio']);
    filterData = this.medalFilter(filterData, form['selos']);

    return filterData;
  }

  private boardFilter(filterData, board) {
    return board == 'todos' ? filterData : filterData.filter(elem => elem.board == board);
  }

  private managementFilter(filterData, management) {
    return management == 'todos' ? filterData : filterData.filter(elem => elem.management == management);
  }

  private decisionLevelFilter(filterData, decisionLevel) {
    return decisionLevel == 'todos' ? filterData : filterData.filter(elem => elem.decisionLevel == decisionLevel);
  }

  private medalFilter(filterData, medals) {
    let selectMedal = [];
    for (let medal in medals) if(medals[medal]) selectMedal.push(medal);
    return selectMedal.length == 0 ? filterData : filterData.filter(elemDado => selectMedal.find(elemSelo => elemDado.medal == elemSelo));
  }

  private errorHandler<T>(message: string, result: T) {
    return (error: any): Observable<any> => {
      console.error(`${message}: ${error.message}`);
      return of(result as T);
    };
  }
}
