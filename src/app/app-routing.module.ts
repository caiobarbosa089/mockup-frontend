import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules, Router} from '@angular/router';
import { ApplicationStateService } from './services/application-state.service';

const desktop_routes: Routes = [
  // { path: '', loadChildren: () => import('./desktop/desktop.module').then(mod => mod.DesktopModule), }
];

const mobile_routes: Routes = [
  // { path: '', loadChildren: () => import('./mobile/mobile.module').then(mod => mod.MobileModule), }
];

@NgModule({
  imports: [RouterModule.forRoot(desktop_routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
  constructor(private router: Router,
  private applicationStateService: ApplicationStateService) {
    if (this.applicationStateService.getIsMobileResolution()) router.resetConfig(mobile_routes)
  }
}
