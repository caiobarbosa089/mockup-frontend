import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { DesktopModule } from './desktop/desktop.module'
import { MobileModule } from './mobile/mobile.module'

import { DashboardService } from './services/dashboard.service';

import { AppComponent } from './app.component';
import { ApplicationStateService } from './services/application-state.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FilterService } from './services/filter.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DesktopModule,
    MobileModule,
    BrowserAnimationsModule
  ],
  providers: [ApplicationStateService, DashboardService, FilterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
